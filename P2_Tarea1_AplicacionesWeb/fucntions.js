
document.querySelector('.boton').addEventListener('click', show);

function show(){

    
    const xhttp = new XMLHttpRequest();

    xhttp.open('GET', 'JSON_estudiantes.json', true);
    xhttp.send();
    xhttp.onreadystatechange = function(){

        if(this.readyState == 4 && this.status == 200){
            let datos = JSON.parse(this.responseText);
            let respuesta = document.querySelector('#datos');
            respuesta.innerHTML = '';

            for(let item of datos){
                respuesta.innerHTML += `
                <tr>
                    <td>${item.cedula}</td>
                    <td>${item.nombre}</td>
                    <td>${item.correo}</td>
                    <td>${item.direccion}</td>
                    <td>${item.telefono}</td>
                    <td>${item.curso}</td>
                    <td>${item.paralelo}</td>
                </tr>
                `
            }
        }
    }
}