function converter(){
    var number = document.getElementById("numberid").value;
    var numberdecimal = parseInt(number, 8);
    var newWindow = window.open("", "", "width=500, height=150");
    newWindow.alert("El numero octal ingresado en decimal es: " + numberdecimal);
}

function Calc(){
    var numOne = parseInt(document.getElementById("input_firstvalue").value);
    var numTwo = parseInt(document.getElementById("input_secondvalue").value);
    Calcular(numOne, numTwo);
}

function Calcular(numOne, numTwo){
    for(i=0; i<4; i++){
        switch(i){
            case 0:
                alert("La suma de los dígitos es: " + (numOne+numTwo));
                break;
            case 1:
                alert("La resta de los dígitos es: " + (numOne-numTwo));
                break;
            case 2:
                alert("La multiplicación de los dígitos es: " + (numOne*numTwo));
                break;
            case 3:
                alert("La división de los dígitos es: " + (numOne/numTwo));
                break;
        }
    }
}