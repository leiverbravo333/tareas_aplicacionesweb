// Primer Punto
var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
var month = [];

function showMonths() {
    for(i = 0; i < 12; i++){
        month.push(meses[i]);
        h3.textContent = month.join(" ");
    }
}

// Segundo Punto

var obj = [];

//Creación de objeto
function Producto_alimenticio(codigo, nombre, precio) {
    this.codigo = codigo;
    this.nombre = nombre;
    this.precio = precio;
    this.imprimeDatos = function(){
        obj.push(["Producto: "+"Código: " + this.codigo + ", Nombre: " + this.nombre + ", Precio: " + this.precio]);
        texto.textContent = obj.join("\n");
        
    };
    
}

//Array con los objetos instanciados
var arrayTresObjetos = [
    watermelon = new Producto_alimenticio("f34", "Sandía", 2.1),
    tomato = new Producto_alimenticio("b32", "Tomate", 1.3),
    avocado = new Producto_alimenticio("c56", "Aguacate", 2.3)
]

// Función con el for para recorrer los objetos
function mostrarObj(){
    for(j=0; j<3; j++){
        arrayTresObjetos[j].imprimeDatos();
    }
}